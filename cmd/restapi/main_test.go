package main

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/assert/v2"
)

func TestPing(t *testing.T) {
	gin.SetMode(gin.ReleaseMode)
	loadConfig()
	router := setupRouter()
	gin.SetMode(gin.ReleaseMode)
	responserecorder := httptest.NewRecorder()
	request, _ := http.NewRequest("GET", "/v1/ping", nil)
	router.ServeHTTP(responserecorder, request)

	pingmessage := k.String("message")

	assert.Equal(t, 200, responserecorder.Code)
	if pingmessage == "" {
		assert.Equal(t, "{\"message\":\"\"}", responserecorder.Body.String())
	} else {
		assert.Equal(t, "{\"message\":\""+pingmessage+"\"}", responserecorder.Body.String())
	}

}
