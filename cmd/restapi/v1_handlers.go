package main

import "github.com/gin-gonic/gin"

//Ping Just returns a string
// @Summary Return json string
// @Description Ping api endpoint
// @Produce application/json
// @Success 200
// @Failure 404
// @Router /ping [get]
func Ping(c *gin.Context) {
	c.JSON(200, gin.H{
		"message": k.String("message"),
	})
}
