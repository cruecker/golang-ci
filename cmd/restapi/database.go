package main

import (
	"log"
	"sync"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func connectDB(connectionString string) *gorm.DB {

	log.Println("Connecting to db")
	var retrycount int
	var conn bool
	var err error
	var db *gorm.DB

	for conn == false {
		if retrycount != 0 {
			time.Sleep(2 * time.Second)
			log.Printf("Retrying database connection %+v\n", retrycount)
		}

		db, err = gorm.Open(postgres.Open(connectionString), &gorm.Config{})

		if err == nil {
			conn = true
		} else {
			log.Println(err)
			retrycount++
		}

	}
	log.Println("DB Connected")
	return db
}

func loadModelsIntoDb(connectionString string, wg *sync.WaitGroup) {
	defer wg.Done()

	db := connectDB(connectionString)

	db.AutoMigrate(&ExampleModel{})
}

func setDatabaseContext(connectionString string) gin.HandlerFunc {
	db := connectDB(connectionString)

	return func(c *gin.Context) {
		c.Set("DB", db)
		c.Next()
	}
}

//String database model
type ExampleModel struct {
	gorm.Model
	String string `gorm:"type:varchar(100)"`
}
