package main

import (
	"log"
	"os"
	"strings"
	"sync"

	_ "gitlab.com/cruecker/golang-ci/api"

	"github.com/gin-gonic/gin"
	"github.com/knadh/koanf"
	"github.com/knadh/koanf/parsers/yaml"
	"github.com/knadh/koanf/providers/env"
	"github.com/knadh/koanf/providers/file"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

// @Title Example REST API
// @Version v1.0
// @Description Example REST API
// @BasePath /v1

var k = koanf.New(".")
var env_var_prefix = "ENV_"

func ensureDir(path string) {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		err = os.Mkdir(path, 0775)
		if err != nil {
			panic(err)
		}
	}
}

func loadConfig() {

	//check in config directory of package for testing
	if err := k.Load(file.Provider("../../configs/config.yaml"), yaml.Parser()); err != nil {
		log.Printf("Error loading config, %v", err)
	}

	//check for config in /etc
	if err := k.Load(file.Provider("/etc/rest-api/config.yaml"), yaml.Parser()); err != nil {
		log.Printf("Error loading config, %v", err)
	}
	k.Load(env.Provider(env_var_prefix, ".", func(s string) string {
		return strings.Replace(strings.ToLower(
			strings.TrimPrefix(s, env_var_prefix)), "_", ".", -1)
	}), nil)

}

//setupRouter configures middlewares and routes
func setupRouter() *gin.Engine {
	router := gin.Default()

	connectionString := "host=" + k.String("db.address") + " user=" + k.String("db.user") + " password=" + k.String("db.password") + " dbname=" + k.String("db.database") + " port=5432 sslmode=disable TimeZone=Europe/Berlin"

	router.Use(setDatabaseContext(connectionString))
	router.MaxMultipartMemory = 1 << 20
	v1 := router.Group("/v1")
	{
		v1.GET("/ping", Ping)

	}

	//setup swagger documentation router
	url := ginSwagger.URL(k.String("external_url") + "/doc/doc.json")
	docrouter := router.Group("/doc")
	{
		docrouter.GET("*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))
	}
	return router
}

func main() {
	loadConfig()

	router := setupRouter()

	connectionString := "host=" + k.String("db.address") + " user=" + k.String("db.user") + " password=" + k.String("db.password") + " dbname=" + k.String("db.database") + " port=5432 sslmode=disable TimeZone=Europe/Berlin"

	//Wait for database connection to succeed
	var mWaiter sync.WaitGroup
	mWaiter.Add(1)
	go loadModelsIntoDb(connectionString, &mWaiter)

	router.Run() // listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")
}
