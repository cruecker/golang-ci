# Golang-CI

 [![pipeline status](https://gitlab.com/cruecker/golang-ci/badges/master/pipeline.svg)](https://gitlab.com/cruecker/golang-ci/-/commits/master)

This is an exemplary Golang REST API packaged in Docker to showcase a simple CI pipeline with automated unit testing and docker image building  

<br>

___

**Contents**
- [Golang-CI](#golang-ci)
  - [Configuration](#configuration)
    - [**Config file example**](#config-file-example)
    - [**ENV vars**](#env-vars)
  - [Deployment](#deployment)
    - [**Compose example**](#compose-example)

___

<br>

## Configuration

This application looks for its configuration in:
- ../../configs/config.yaml (For development testing)
- /etc/rest-api/config.yaml
- in environment vars prefixed with ENV_

<br>

### **Config file example**
    db:
      address: database.hostname
      password: localtesting
      database: restapi
      user: restapi

    external_url: http://localhost:8080
    message: "This value is from the config file"

<br>

### **ENV vars**
| *Variable*         | *Description*                       |
|--------------------|-------------------------------------|
| `ENV_MESSAGE`      | Message displayed in /ping endpoint |
| `ENV_DB_ADDRESS`   | Database address for app            |
| `ENV_DB_DATABASE`  | Name of database                    |
| `ENV_DB_USER`      | Username for database               |
| `ENV_DB_PASSWORD`  | Password for database user          |
| `ENV_EXTERNAL_URL` | Password for database user          |
___   

<br>

## Deployment
### **Compose example**
See [docker-compose.yml](scripts/docker-compose.yml)

