module gitlab.com/cruecker/golang-ci

go 1.16

require (
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751
	github.com/gin-gonic/gin v1.7.1
	github.com/go-playground/assert/v2 v2.0.1
	github.com/knadh/koanf v0.16.0
	github.com/swaggo/files v0.0.0-20190704085106-630677cd5c14
	github.com/swaggo/gin-swagger v1.3.0
	github.com/swaggo/swag v1.7.0
	gorm.io/driver/postgres v1.0.8
	gorm.io/gorm v1.21.9
)
